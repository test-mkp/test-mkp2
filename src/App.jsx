import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import TransactionList from "./pages/TransactionList";
import { ThemeProvider } from "@/components/ui/theme-provider";
import TransactionChart from "./pages/TransactionChart";
import Layout from "./components/layout/Layout";
import Dashboard from "./pages/Dashboard";
import TransactionDetail from "./pages/TransactionDetail";

function App() {
  return (
    <ThemeProvider defaultTheme="dark" storageKey="vite-ui-theme">
      <Router>
        <Routes>
          <Route element={<Layout />}>
            <Route path="/" element={<Dashboard />} />
            <Route path="/transaction" element={<TransactionList />} />

            <Route path="/detail/:id" element={<TransactionDetail />} />

            <Route path="/Chart" element={<TransactionChart />} />
          </Route>
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;

import React from "react";

const Loading = () => {
  return (
    <div className="flex h-[500px] justify-center items-center">
      <div className="custom-loader"></div>;
    </div>
  );
};

export default Loading;

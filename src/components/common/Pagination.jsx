import React from "react";
import { GrPrevious, GrNext } from "react-icons/gr";

const Pagination = ({
  currentPage,
  setCurrentPage,
  totalItems,
  itemsPerPage,
}) => {
  const totalPages = Math.ceil(totalItems / itemsPerPage);

  const handlePreviousClick = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const handleNextClick = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
    }
  };

  return (
    <div className="flex justify-end items-center text-sm mt-4 gap-4 text-white">
      <div>
        <button
          onClick={handlePreviousClick}
          className="w-20 p-2 bg-[#1d01c9] hover:bg-blue-950 rounded-md flex justify-center items-center"
        >
          <GrPrevious /> Previous
        </button>
      </div>
      <div className="page-number">
        {totalPages === 0 ? "" : `${currentPage} of ${totalPages}`}
      </div>
      <div>
        <button
          onClick={handleNextClick}
          className="w-20 p-2 bg-[#1d01c9] hover:bg-blue-950 rounded-md flex justify-around items-center"
        >
          <span>Next</span>
          <span>
            <GrNext />
          </span>
        </button>
      </div>
    </div>
  );
};

export default Pagination;

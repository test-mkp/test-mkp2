import React from "react";
import { Link } from "react-router-dom";
import { BiDetail } from "react-icons/bi";
import useTransactions from "@/hooks/useTransactions";

const TableSmallComp = ({ data, action, currentPage }) => {
  const { getStatusColor, formatRupiah, formatDate, dataPerPage } =
    useTransactions();

  const startIndex = (currentPage - 1) * dataPerPage;

  return (
    <div>
      {data.length > 0 ? (
        data.map((item, index) => {
          const {
            id,
            customerName,
            amount,
            date,
            status,
            paymentMethod,
            location,
          } = item;

          return (
            <div
              key={id}
              className="bg-[#21222D] flex flex-col rounded-md p-4 mb-2 gap-2 shadow-sm "
            >
              <div>
                <div className="flex justify-between ">
                  <p>No</p> <p>{startIndex + index + 1}</p>
                </div>
                <div className="flex justify-between">
                  <p>Customer Name</p> <p>{customerName}</p>
                </div>
                <div className="flex justify-between">
                  <p>Amount</p> <p>{formatRupiah(amount)}</p>
                </div>
                <div className="flex justify-between">
                  <p>Date</p> <p>{formatDate(date)}</p>
                </div>
                <div className="flex justify-between">
                  <p>Status</p>{" "}
                  <p className={getStatusColor(status)}>{status}</p>
                </div>
                <div className="flex justify-between">
                  <p>Paymen Method</p> <p>{paymentMethod}</p>
                </div>
                <div className="flex justify-between">
                  <p>Location</p> <p>{location}</p>
                </div>
                {!action ? (
                  ""
                ) : (
                  <div className="flex justify-between">
                    <p>Action</p>{" "}
                    <Link to={`/detail/${id}`}>
                      <BiDetail />
                    </Link>
                  </div>
                )}
              </div>
            </div>
          );
        })
      ) : (
        <div>
          <p className="text-center text-white">Transactions not found</p>
        </div>
      )}
    </div>
  );
};

export default TableSmallComp;

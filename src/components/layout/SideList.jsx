import React, { useState } from "react";
import { RiHomeFill } from "react-icons/ri";
import { FaUser, FaChartBar } from "react-icons/fa";
import { GrTransaction } from "react-icons/gr";
import { Link, useLocation } from "react-router-dom";

const SideList = ({ toggleCollapse }) => {
  const sideLList = [
    { title: "Dashboard", path: "/", icon: <RiHomeFill /> },
    { title: "Transaction", path: "/transaction", icon: <GrTransaction /> },
    { title: "Chart", path: "/chart", icon: <FaChartBar /> },
  ];

  const location = useLocation();
  return (
    <div>
      <ul className="flex flex-col gap-3 ">
        {sideLList?.map((item, sidebar) => {
          // membuat variabel isActive untuk menghandle ketika path yang sedang aktif maka bisa kita atur background dan lainya
          const isActive = location.pathname === item.path;
          return (
            <Link to={item.path} key={sidebar}>
              <li
                className={`flex items-center gap-2  px-2 py-2 rounded-md ${
                  isActive && !toggleCollapse
                    ? "bg-[#1d01c9] text-gray-300"
                    : ""
                }  ${
                  toggleCollapse
                    ? isActive
                      ? " justify-center text text-[#1d01c9] rounded-none pl-[9px] border-r-2 border-r-[#1d01c9]"
                      : " justify-center text"
                    : "mx-5"
                }`}
              >
                {toggleCollapse ? (
                  <span>{item.icon}</span>
                ) : (
                  <div className="flex items-center gap-2">
                    <span>{item.icon}</span>
                    <span>{item.title}</span>
                  </div>
                )}
              </li>
            </Link>
          );
        })}
      </ul>
    </div>
  );
};

export default SideList;

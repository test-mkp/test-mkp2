import React from "react";
import { Link } from "react-router-dom";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { BiDetail } from "react-icons/bi";
import useTransactions from "@/hooks/useTransactions";

const TableComp = ({ data, action, currentPage }) => {
  const { formatDate, formatRupiah, getStatusColor, dataPerPage } =
    useTransactions();

  const startIndex = (currentPage - 1) * dataPerPage;

  return (
    <div>
      <Table className="bg-[#21222D] rounded-md">
        <TableHeader>
          <TableRow>
            <TableHead className="w-10">No</TableHead>
            <TableHead className="">Customer Name</TableHead>
            <TableHead className=""> Amount</TableHead>
            <TableHead className=""> Date</TableHead>
            <TableHead className="">Status</TableHead>
            <TableHead className="">Paymen Method</TableHead>
            <TableHead className="">Location</TableHead>
            {!action ? "" : <TableHead className="">Action</TableHead>}
          </TableRow>
        </TableHeader>
        <TableBody>
          {data.length > 0 ? (
            data.map((item, index) => {
              const {
                id,
                customerName,
                amount,
                date,
                status,
                paymentMethod,
                location,
              } = item;

              return (
                <TableRow key={id} className="text-white">
                  <TableCell className="font-medium">
                    {startIndex + index + 1}
                  </TableCell>
                  <TableCell>{customerName}</TableCell>
                  <TableCell>{formatRupiah(amount)}</TableCell>
                  <TableCell>{formatDate(date)}</TableCell>
                  <TableCell className={getStatusColor(status)}>
                    {status}
                  </TableCell>
                  <TableCell>{paymentMethod}</TableCell>
                  <TableCell>{location}</TableCell>
                  {!action ? (
                    ""
                  ) : (
                    <TableCell>
                      <Link to={`/detail/${id}`}>
                        <BiDetail />
                      </Link>
                    </TableCell>
                  )}
                </TableRow>
              );
            })
          ) : (
            <TableRow>
              <TableCell colSpan="8" className="text-center text-white">
                Transactions not found
              </TableCell>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </div>
  );
};

export default TableComp;

import React, { useEffect, useState } from "react";
import SideList from "./SideList";
import { BiMenu } from "react-icons/bi";

const Sidebar = ({ toggleCollapse, setToggleCollapse }) => {
  // fungsi untuk mengatur sidebar terbuka/tertutup di ukuran layar <=900
  const handleResize = () => {
    if (window.innerWidth <= 970) {
      setToggleCollapse(true);
    } else {
      setToggleCollapse(false);
    }
  };
  useEffect(() => {
    //agar ketika ukuran layar kecil dan ketika browser di refresh sidebarnya tetap menutup
    handleResize();
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);
  return (
    <div
      className={`fixed text-[#87888C] z-50 h-full transition duration-300 ease-in-out border border-r-[#2C2D33] ${
        toggleCollapse ? "w-16" : "w-[200px]"
      } bg-[#010317]`}
    >
      <div className="flex items-center text-center py-3 px-4 text-xl font-semibold">
        {toggleCollapse ? "" : <h3 className="ml-1 ">Transactions</h3>}

        <button
          onClick={() => setToggleCollapse(!toggleCollapse)}
          className={`px-2 relative text-gray-300 bg-[#1d01c9] rounded-sm py-2 ml-3 hover:bg-blue-950 transition duration-300 ease-in-out ${
            toggleCollapse ? "right-4" : "left-20"
          }`}
        >
          <BiMenu />
        </button>
      </div>
      <div className="mt-4">
        <SideList toggleCollapse={toggleCollapse} />
      </div>
    </div>
  );
};

export default Sidebar;

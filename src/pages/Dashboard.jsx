import Loading from "@/components/common/Loading";
import useTransactions from "@/hooks/useTransactions";

import React from "react";
import { FaCheckSquare } from "react-icons/fa";
import {
  MdPendingActions,
  MdOutlineCancelPresentation,
  MdRecycling,
} from "react-icons/md";
import { Link } from "react-router-dom";
import TableSmallComp from "@/components/layout/TableSmallComp";
import TableComp from "@/components/layout/TableComp";

const Dashboard = () => {
  const { calculateStatusTransaction, isLoading } = useTransactions();
  const { transactionsList } = useTransactions();
  const data = calculateStatusTransaction();

  // mencari total dari parameter yang dikirimkan dan menggunakan data calculateStatusTransaction
  const findTotal = (name) => {
    const status = data.find((item) => item?.name === name);
    return status ? status?.value : 0;
  };

  // mengambil 5 data date terbaru
  const sortLatestTransaction = [...transactionsList]
    .sort((a, b) => b.date - a.date)
    .slice(0, 5);

  const dashList = [
    {
      title: "Processing",
      path: "/transaction?status=Processing",
      total: findTotal("Processing"),
      icon: <MdRecycling />,
    },
    {
      title: "Cancelled",
      path: "/transaction?status=Cancelled",
      total: findTotal("Cancelled"),
      icon: <MdOutlineCancelPresentation />,
    },
    {
      title: "Pending",
      path: "/transaction?status=Pending",
      total: findTotal("Pending"),
      icon: <MdPendingActions />,
    },
    {
      title: "Completed",
      path: "/transaction?status=Processing",
      total: findTotal("Completed"),
      icon: <FaCheckSquare />,
    },
  ];

  if (isLoading || !data.length) return <Loading />;

  return (
    <div className="flex flex-col   mx-6">
      <h1 className=" font-bold mt-6 text-lg">Dashboard</h1>
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-2  h-[30%] w-full mt-6">
        {/* menampilkan dashlist */}
        {dashList?.map((item, index) => {
          const { title, path, total, icon } = item;
          return (
            <div
              className="text-black bg-[#2B2B36] h-28 rounded-md flex justify-between items-center px-3"
              key={index}
            >
              <div className="text-white">
                <h3 className="text-sm">{title}</h3>
                <p className="font-bold text-xl">{total}</p>
              </div>
              <Link
                to={path}
                className="flex justify-center items-center h-12 w-12 bg-[#1d01c9] hover:bg-blue-950 rounded-md text-gray-300 text-xl"
              >
                {icon}
              </Link>
            </div>
          );
        })}
      </div>
      {/* menampilkan data table transaksi terbaru */}
      <h1 className="text-lg font-bold pt-4 pb-6">Latest Transactions</h1>
      <div className="hidden md:block">
        <TableComp
          data={sortLatestTransaction}
          action={false}
          currentPage={1}
        />
      </div>
      <div className="block md:hidden">
        <TableSmallComp
          data={sortLatestTransaction}
          action={false}
          currentPage={1}
        />
      </div>
    </div>
  );
};

export default Dashboard;

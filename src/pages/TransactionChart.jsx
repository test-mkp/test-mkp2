import Loading from "@/components/common/Loading";
import useTransactions from "@/hooks/useTransactions";
import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  LabelList,
  Cell,
} from "recharts";
const TransactionChart = () => {
  const { calculateStatusTransaction } = useTransactions();

  const data = calculateStatusTransaction();

  // getColor berfungsi untuk menentukan warna berdasarkan status yang dikirimkan
  const getColor = (status) => {
    switch (status) {
      case "Pending":
        return "#FFA500";
      case "Processing":
        return "#FFFF00";
      case "Cancelled":
        return "#FF0000";
      case "Completed":
        return "#008000";
      default:
        return "#8884d8";
    }
  };
  if (!data) return <Loading />;
  return (
    <div className=" my-20 mx-6">
      <h2 className="text-xl t font-bold mb-4">
        Transaction Status Distribution
      </h2>
      <div className="flex justify-center sm:pt-16">
        {/* menampilkan Bar Chart */}
        <BarChart
          width={600}
          height={400}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="2 2" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          {/* <Legend /> */}
          <Bar dataKey="value">
            <LabelList dataKey="value" position="top" />
            {data.map((entry, index) => (
              <Cell key={index} fill={getColor(entry.name)} />
            ))}
          </Bar>
        </BarChart>
      </div>
    </div>
  );
};

export default TransactionChart;

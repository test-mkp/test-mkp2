import React, { useState } from "react";
import Dropdown from "@/components/common/Dropdown";
import Pagination from "@/components/common/Pagination";
import InputSearch from "@/components/common/InputSearch";
import useTransactions from "@/hooks/useTransactions";
import Loading from "@/components/common/Loading";
import TableSmallComp from "@/components/layout/TableSmallComp";
import TableComp from "@/components/layout/TableComp";

const sortItems = [
  { label: "None", value: "id" },
  { label: "Date (Asc)", value: "date-asc" },
  { label: "Date (Desc)", value: "date-desc" },
  { label: "Amount (Asc)", value: "amount-asc" },
  { label: "Amount (Desc)", value: "amount-desc" },
];

const TransactionList = () => {
  const [sortName, setSortName] = useState("None");

  const {
    //jika nilai null maka pakai nilai default yaitu []
    originalTransactionsList = [],
    labelStatus,
    setLabelStatus,
    labelLocation,
    setLabelLocation,
    labelPaymentMethod,
    setLabelPaymentMethod,
    isLoading,
    setSearch,
    currentPage,
    setCurrentPage,
    dataPerPage,
    sortTransaction,
    filteredData,
    filterStatus,
  } = useTransactions();

  const { paginatedData, totalFilteredItems } = filteredData();

  const statusItems = [
    { label: "Any", value: "Any" },
    ...[
      ...new Set((originalTransactionsList || []).map((value) => value.status)),
    ].map((value) => ({ label: value, value: value })),
  ];
  const paymentMethod = [
    { label: "Any", value: "Any" },
    ...[
      ...new Set(
        (originalTransactionsList || []).map((value) => value.paymentMethod)
      ),
    ].map((value) => ({ label: value, value: value })),
  ];
  const location = [
    { label: "Any", value: "Any" },
    ...[
      ...new Set(
        (originalTransactionsList || []).map((value) => value.location)
      ),
    ].map((value) => ({ label: value, value: value })),
  ];

  if (isLoading) return <Loading />;

  return (
    <div className="mx-6 my-20">
      <h1 className="text-xl font-bold mb-4">Transaction List</h1>
      <div className="grid md:grid-cols-2 md:w-2/3 gap-2 mt-4">
        {/* dropdown untuk mengurutkan berdasarkan date/ amount(descending & ascending) */}
        <Dropdown
          label="Sorted by"
          item={sortItems}
          onSelect={sortTransaction}
          setName={setSortName}
          name={sortName}
        />
        {/* dropdown untuk memfilter data berdasarkan statusnya */}
        <Dropdown
          label="Filter by Status"
          item={statusItems}
          onSelect={(value) => filterStatus(value, "status")}
          setName={setLabelStatus}
          name={labelStatus}
        />
        {/* dropdown untuk memfilter data berdasarkan lokasi */}
        <Dropdown
          label="Filter by location"
          item={location}
          onSelect={(value) => filterStatus(value, "location")}
          setName={setLabelLocation}
          name={labelLocation}
        />
        {/* dropdown untuk memfilter data berdasarkan metode pembayaran */}
        <Dropdown
          label="Filter by Payment"
          item={paymentMethod}
          onSelect={(value) => filterStatus(value, "paymentMethod")}
          setName={setLabelPaymentMethod}
          name={labelPaymentMethod}
        />
      </div>
      <div className="flex flex-col-reverse md:flex-row pb-3">
        {/* inputsearch berfungsi untuk searching */}
        <InputSearch setSearch={setSearch} setCurrentPage={setCurrentPage} />
      </div>
      <div className="hidden md:block">
        {/* TableComp ini adalah komponen yang digunakan untuk menampilkan data berbentuk table*/}
        <TableComp
          data={paginatedData}
          action={true}
          currentPage={currentPage}
        />
      </div>
      <div className="block md:hidden">
        {/* TableSmallComp ini adalah komponen untuk menampilkan data table di ukuran layar kecil */}
        <TableSmallComp
          data={paginatedData}
          action={true}
          currentPage={currentPage}
        />
      </div>
      <Pagination
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        totalItems={totalFilteredItems}
        itemsPerPage={dataPerPage}
      />
    </div>
  );
};

export default TransactionList;

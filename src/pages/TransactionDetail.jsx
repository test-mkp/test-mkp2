import BreadcrumbComp from "@/components/common/BreadcrumbComp";
import Loading from "@/components/common/Loading";
import useTransactions from "@/hooks/useTransactions";
import React from "react";
import { useParams } from "react-router";

const TransactionDetail = () => {
  const { transactionsList, getStatusColor } = useTransactions();
  // mengambil id  useParams
  const param = useParams();
  const id = param.id;

  // memfilter data yang memiiki id yang sama dengan id yang kita ambil dari useParams
  const detailTransaction = transactionsList.find(
    (user) => user?.id === parseInt(id)
  );
  if (!detailTransaction) {
    return <Loading />;
  }

  // melakuan destructuring untuk mengambil property yang ada di dalam detailTransaction
  const { customerName, amount, date, status, paymentMethod, location } =
    detailTransaction;

  return (
    <div className="container mx-3 p-4 mt-4  rounded">
      <div className="py-3">
        {/* menampilkan breadcrumb */}
        <BreadcrumbComp from="Transaction" now="detail" />
      </div>
      <div className="bg-[#2B2B36] p-4 rounded-md flex flex-col gap-9">
        {/* menampilkan detail transaction */}
        <h2 className="text-xl font-bold mb-3"> Transaction Detail - {id}</h2>
        {/* menampilkan detail transaction */}
        <div className="flex justify-between items-center md:mr-16 gap-10">
          <p className="w-64 font-bold">Customer Name</p>
          <input
            className="w-full rounded-sm p-2"
            type="text"
            disabled
            value={customerName}
          />
        </div>
        <div className="flex justify-between items-center md:mr-16 gap-10">
          <p className="w-64 font-bold">Amount</p>
          <input
            className="w-full rounded-sm p-2"
            type="text"
            disabled
            value={amount}
          />
        </div>
        <div className="flex justify-between items-center md:mr-16 gap-10">
          <p className="w-64 font-bold">Date</p>
          <input
            className="w-full rounded-sm p-2"
            type="text"
            disabled
            value={date}
          />
        </div>
        <div className="flex justify-between items-center md:mr-16 gap-10">
          <p className="w-64 font-bold">Status</p>
          <input
            className={`${getStatusColor(status)} w-full rounded-sm p-2 `}
            type="text"
            disabled
            value={status}
          />
        </div>
        <div className="flex justify-between items-center md:mr-16 gap-10">
          <p className="w-64 font-bold">Paymen Method</p>
          <input
            className="w-full rounded-sm p-2"
            type="text"
            disabled
            value={paymentMethod}
          />
        </div>
        <div className="flex justify-between items-center md:mr-16 gap-10">
          <p className="w-64 font-bold">Location</p>
          <input
            className="w-full rounded-sm p-2"
            type="text"
            disabled
            value={location}
          />
        </div>
      </div>
    </div>
  );
};

export default TransactionDetail;

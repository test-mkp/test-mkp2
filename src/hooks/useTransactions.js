import { dataTransaction, url_data } from "@/lib/constant";
import axios from "axios";
import { format, parse } from "date-fns";
import { useEffect, useState } from "react";

const useTransactions = () => {
  const [originalTransactionsList, setOriginalTransactionsList] = useState([]);
  const [transactionsList, setTransactionsList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [search, setSearch] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [labelStatus, setLabelStatus] = useState("Any");
  const [labelLocation, setLabelLocation] = useState("Any");
  const [labelPaymentMethod, setLabelPaymentMethod] = useState("Any");
  const [sortName, setSortName] = useState("None");
  const [filterName, setFilterName] = useState("Any");
  const dataPerPage = 5; // Jumlah pengguna per halaman

  //mengambil data dari url yang ada, jika urlnya error/data tidak bisa diambil dari url maka data yang dipakai adalah data dari dataTransaction yang ada di dalam lib/constant
  const getTransactionsData = async () => {
    await axios
      .get(url_data)
      .then((res) => {
        setOriginalTransactionsList(res?.data || dataTransaction);
        setTransactionsList(res?.data || dataTransaction);
      })
      .catch((error) => {
        console.log(error);
        setOriginalTransactionsList(dataTransaction);
        setTransactionsList(dataTransaction);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  // ini adalah fungsi untuk mengurutkan transactionList sesuai dengan parameter yang dikirm(misal date atau amount)
  const sortTransaction = (param) => {
    let sortData = [];
    if (param === "date-asc") {
      sortData = [...transactionsList].sort((a, b) => a.date - b.date);
    } else if (param === "date-desc") {
      sortData = [...transactionsList].sort((a, b) => b.date - a.date);
    } else if (param === "amount-asc") {
      sortData = [...transactionsList].sort((a, b) => a.amount - b.amount);
    } else if (param === "amount-desc") {
      sortData = [...transactionsList].sort((a, b) => b.amount - a.amount);
    } else if (param === "id") {
      sortData = [...transactionsList].sort((a, b) => a.id - b.id);
    }
    setTransactionsList(sortData);
    setCurrentPage(1);
  };

  //  funnction yang berfungsi untuk memfilter status sesuai dengan param, difilternya menggunakan ternary, jadi jika namenya "status" dan paramnyanya "Any" maka mucnulkan semuanya, kalau namenya status tapi paramnya bukan Any maka filter item.status yang sama dg param, dan kalau namenya bukan status maka dicek lagi dan penjelasnnya sama seperti di atas.
  const filterStatus = (param, name = "status") => {
    setTransactionsList(
      originalTransactionsList.filter(
        (item) =>
          (name === "status"
            ? param === "Any"
              ? item.status.includes("")
              : item.status === param
            : labelStatus === "Any"
            ? item.status.includes("")
            : item.status === labelStatus) &&
          (name === "location"
            ? param === "Any"
              ? item.location.includes("")
              : item.location === param
            : labelLocation === "Any"
            ? item.location.includes("")
            : item.location === labelLocation) &&
          (name === "paymentMethod"
            ? param === "Any"
              ? item.paymentMethod.includes("")
              : item.paymentMethod === param
            : labelPaymentMethod === "Any"
            ? item.paymentMethod.includes("")
            : item.paymentMethod === labelPaymentMethod)
      )
    );
    setCurrentPage(1);
  };

  //ini berfungsi untuk memfiler data berdasarkan customerName yang akan digunakan untuk fitur pencarian
  const filteredData = () => {
    const filterData = transactionsList.filter((item) => {
      return search.toLowerCase() === ""
        ? item
        : item.customerName.toLowerCase().includes(search.toLowerCase());
    });
    // Menghitung indeks pengguna yang akan ditampilkan di halaman saat ini, dan yang digunakan adalah data yang sudah di filter untuk fitur pencarian
    const paginatedData = filterData.slice(
      (currentPage - 1) * dataPerPage,
      currentPage * dataPerPage
    );
    return { paginatedData, totalFilteredItems: filterData.length };
  };

  // function ini digunakan untuk mengcalculate status jadi tujuannya untuk mencari tau jumlah dari tiap status yang akan digunakan untuk kebutuhan chart
  const calculateStatusTransaction = () => {
    const statusCount = transactionsList.reduce((acc, transaction) => {
      acc[transaction.status] = (acc[transaction.status] || 0) + 1;
      return acc;
    }, {});
    return Object.keys(statusCount).map((status) => ({
      name: status,
      value: statusCount[status],
    }));
  };

  // Function untuk menentukan warna berdasarkan nama status
  const getStatusColor = (status) => {
    switch (status) {
      case "Pending":
        return "text-orange-500";
      case "Processing":
        return "text-yellow-500";
      case "Cancelled":
        return "text-red-500";
      case "Completed":
        return "text-green-500";
      default:
        return "text-white";
    }
  };

  // Function ini digunakan untuk memformat tanggal
  const formatDate = (dateString) => {
    const date = parse(dateString, "yyyyMMdd", new Date());
    return format(date, "dd MMMM yyyy");
  };

  // Function ini digunakan untuk memformat amount
  const formatRupiah = (amount) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(amount);
  };

  useEffect(() => {
    getTransactionsData();
  }, []);

  return {
    labelStatus,
    setLabelStatus,
    labelLocation,
    setLabelLocation,
    labelPaymentMethod,
    setLabelPaymentMethod,
    originalTransactionsList,
    transactionsList,
    setTransactionsList,
    isLoading,
    search,
    setSearch,
    currentPage,
    setCurrentPage,
    dataPerPage,
    sortTransaction,
    filteredData,
    sortName,
    setSortName,
    filterName,
    setFilterName,
    filterStatus,
    calculateStatusTransaction,
    getStatusColor,
    formatDate,
    formatRupiah,
  };
};
export default useTransactions;
